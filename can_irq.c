#include <linux/interrupt.h>

#include "can.h"
#include "sja1000.h"
#include "can_irq.h"

//-----------------------------------------------------------------------------
#if LINUX_VERSION_CODE > KERNEL_VERSION(2, 6, 18)
irqreturn_t can_irq(int irq, void *dev_id)
#else
irqreturn_t can_irq(int irq, void *dev_id, struct pt_regs *notused)
#endif
{
	/* Реализация функции обслуживания прерывания */
	int channel, port;
	unsigned char mode;
	struct can_t *can;
	struct can_rx_fifo_t *rx_fifo;
	struct can_tx_fifo_t *tx_fifo;
	CAN_data packet;
	irqreturn_t ret = IRQ_NONE;
	union CAN_interrupt_t interrupt;
	struct timeval timestamp;
	unsigned long flags;
	union CAN_status_t status;
	unsigned char res_intr;

	can = (struct can_t *)dev_id;

	/* Захватываем спин и запрещаем локальные прерывания */
	spin_lock_irqsave(&can->irq_lock, flags);
	while( 1 )
	{
		res_intr = 0;
		for(channel = 0; channel < CAN_NUM_CHANNEL; channel++)
		{
			port = can->wPorts[channel];

			interrupt.byte = get_interrupt(port);
			mode = get_work_mode(can->wPorts[channel]);
			if( mode == BasicCAN )
			{
				interrupt.byte = interrupt.byte & 0x1f;
			}
			res_intr = res_intr | interrupt.byte;

			if( interrupt.byte != 0 )
			{
				/* Прерывание этого канала */
				if( interrupt.bit.ei != 0 )
				{
					/* Ошибка */
					/* Время */
					do_gettimeofday(&timestamp);

					status.byte = get_status(port);

					packet.type = ErrorCAN;
					packet.timestamp = timestamp;
					packet.data[0] = mode;
					packet.data[1] = status.byte;
					packet.data[2] = get_error_code(port);
					packet.data[3] = get_lost_capture(port);

					/* Захватываем спин и запрещаем локальные прерывания */
					spin_lock_irqsave(&can->rx_fifo_lock[channel], flags);

					rx_fifo = can->rx_fifo[channel];
					while( rx_fifo != NULL )
					{
						if( atomic_read(&rx_fifo->len) < CAN_FIFO_SIZE )
						{
							/* Есть место для записи следующего пакета */
							memcpy(&rx_fifo->data[rx_fifo->last], &packet, sizeof(CAN_data));
							rx_fifo->last = (rx_fifo->last + 1) % CAN_FIFO_SIZE;
							atomic_inc(&rx_fifo->len);
						}
						else
						{
							/* Нет места в приемном буфере */
							rx_fifo->overrun = rx_fifo->overrun + 1;
						}
						/* Переходим к следующей очереди */
						rx_fifo = rx_fifo->next;
					}
					/* Освобождаем спин и разрешаем локальные прерывания */
					spin_unlock_irqrestore(&can->rx_fifo_lock[channel], flags);

					/* Сброс контроллера? */
					if( status.bit.bs != 0 )
					{
						/* Захватываем спин и запрещаем локальные прерывания */
						spin_lock_irqsave(&can->tx_fifo_lock[channel], flags);
	
						can->bus_mode[channel] = BUS_OFF;
	
						while( can->tx_fifo[channel] != NULL )
						{
							/* Разблокируем всех клиентов */
							write_replay_error(can, channel);
						}
						if( can->stop_trans[channel] == 1 )
						{
							/* Остановка выдачи */
							can->stopping[channel] = 1;
						}

						/* Освобождаем спин и разрешаем локальные прерывания */
						spin_unlock_irqrestore(&can->tx_fifo_lock[channel], flags);
	
						/* Будим всех ждущих выдачи пакета */
						wake_up(&can->tx_queue[channel]);
					}

					/* Будим всех ждущих принятия пакета */
					wake_up_interruptible(&can->rx_queue[channel]);

					ret = IRQ_HANDLED;
				}
				if( interrupt.bit.ri != 0 )
				{
					/* Принят пакет */
					/* Время */
					do_gettimeofday(&timestamp);
					/* Читаем пакет */
					read_message(port, &packet);
					/* Разрешаем прием следующего кадра */
					set_command(port, 0x04);
					packet.timestamp = timestamp;
	
					/* Захватываем спин и запрещаем локальные прерывания */
					spin_lock_irqsave(&can->rx_fifo_lock[channel], flags);

					/* Записываем принятый пакет в очереди данного канала */
					rx_fifo = can->rx_fifo[channel];
					while( rx_fifo != NULL )
					{
						if( atomic_read(&rx_fifo->len) < CAN_FIFO_SIZE )
						{
							/* Есть место для записи следующего пакета */
							memcpy(&rx_fifo->data[rx_fifo->last], &packet, sizeof(CAN_data));
							rx_fifo->last = (rx_fifo->last + 1) % CAN_FIFO_SIZE;
							atomic_inc(&rx_fifo->len);
						}
						else
						{
							/* Нет места в приемном буфере */
							rx_fifo->overrun = rx_fifo->overrun + 1;
						}
						/* Переходим к следующей очереди */
						rx_fifo = rx_fifo->next;
					}
					/* Освобождаем спин и разрешаем локальные прерывания */
					spin_unlock_irqrestore(&can->rx_fifo_lock[channel], flags);

					/* Будим всех ждущих принятия пакета */
					wake_up_interruptible(&can->rx_queue[channel]);

					ret = IRQ_HANDLED;
				}
				if( interrupt.bit.doi != 0 )
				{
					/* Ошибка (переполнение) */

					/* Захватываем спин и запрещаем локальные прерывания */
					spin_lock_irqsave(&can->rx_fifo_lock[channel], flags);

					rx_fifo = can->rx_fifo[channel];
					while( rx_fifo != NULL )
					{
						rx_fifo->overrun = rx_fifo->overrun + 1;
						/* Переходим к следующей очереди */
						rx_fifo = rx_fifo->next;
					}
					/* Освобождаем спин и разрешаем локальные прерывания */
					spin_unlock_irqrestore(&can->rx_fifo_lock[channel], flags);

					set_command(port, 0x08);

					ret = IRQ_HANDLED;
				}
				if( interrupt.bit.ti != 0 )
				{
					status.byte = get_status(port);

					tx_fifo = NULL;

					/* Захватываем спин и запрещаем локальные прерывания */
					spin_lock_irqsave(&can->tx_fifo_lock[channel], flags);

					if( can->tx_fifo[channel] != NULL )
					{
						if( status.bit.tcs != 1 )
						{
							/* Ошибка выдачи */
							can->tx_fifo[channel]->tx_counter = can->tx_fifo[channel]->tx_counter - 1;
						}
						else
						{
							can->tx_fifo[channel]->tx_counter = TX_COUNTER;
							/* Переходим к выдаче следующего пакета */
							can->tx_fifo[channel]->offset = can->tx_fifo[channel]->offset + sizeof(CAN_data);
							if( can->tx_fifo[channel]->offset == can->tx_fifo[channel]->len )
							{
								tx_fifo = can->tx_fifo[channel];
								/* Статус выдачи */
								if( tx_fifo->private_data != NULL )
								{
									tx_fifo->private_data->status = TRANSMIT_COMPLETE;
									tx_fifo->private_data->len = can->tx_fifo[channel]->offset;
								}
								/* Следующий буфер выдачи */
								can->tx_fifo[channel] = can->tx_fifo[channel]->next;
							}
						}
					}
					if( can->stop_trans[channel] == 1 )
					{
						/* Остановка выдачи */
						can->stopping[channel] = 1;
					}
					else
					{
						/* Выдаем следующий пакет */
						transmit_message(can, channel);
					}
					/* Будим всех ждущих выдачи пакета */
					wake_up(&can->tx_queue[channel]);

					if( tx_fifo != NULL )
					{
						if( tx_fifo->blocking )
						{
							/* Клиент заблокирован */
							tx_fifo->complete = 1;
							/* Будим всех ждущих принятия пакета */
							wake_up_interruptible(&tx_fifo->tx_wait);
						}
						else
						{
							if( tx_fifo->private_data != NULL )
							{
								tx_fifo->private_data->tx_fifo = NULL;
							}
							/* Освобождаем память */
							kfree(tx_fifo->data);
							kfree(tx_fifo);
						}
					}

					/* Освобождаем спин и разрешаем локальные прерывания */
					spin_unlock_irqrestore(&can->tx_fifo_lock[channel], flags);

					ret = IRQ_HANDLED;
				}
			}

		}
		if( res_intr == 0 )
		{
			break;
		}
	}
	/* Освобождаем спин и разрешаем локальные прерывания */
	spin_unlock_irqrestore(&can->irq_lock, flags);

	return IRQ_RETVAL(ret);
}

//-----------------------------------------------------------------------------
void transmit_message(struct can_t *can, int channel)
{
	/* Выдача сообщения по шине CAN с проверкой соответствия режима и счетчика повторов */
	int res;
	CAN_data *data;

	while( can->tx_fifo[channel] != NULL )
	{
		if( can->tx_fifo[channel]->tx_counter == 0 )
		{
			/* Ошибка выдачи пакета после TX_COUNTER повторов */
			write_replay_error(can, channel);
			continue;
		}
		/* Устанавливаем статус выдачи */
		if( can->tx_fifo[channel]->private_data != NULL )
		{
			can->tx_fifo[channel]->private_data->status = TRANSMIT;
			can->tx_fifo[channel]->private_data->len = can->tx_fifo[channel]->offset;
		}
		/* Выдаем следующий пакет */
		data = can->tx_fifo[channel]->data + can->tx_fifo[channel]->offset / sizeof(CAN_data);
		res = write_message(can->wPorts[channel], data);
		if( res == 0 )
		{
			break;
		}
		/* Ошибка выдачи */
		write_replay_error(can, channel);
	}
	return;
}

//-----------------------------------------------------------------------------
void write_replay_error(struct can_t *can, int channel)
{
	/* Разблокирование клиента в случае ошибки */
	struct can_tx_fifo_t *tx_fifo;

	tx_fifo = can->tx_fifo[channel];

	can->tx_fifo[channel] = tx_fifo->next;

	/* Устанавливаем статус выдачи */
	if( tx_fifo->private_data != NULL )
	{
		tx_fifo->private_data->status = TRANSMIT_ERROR;
		tx_fifo->private_data->len = tx_fifo->offset;
	}

	if( tx_fifo->blocking )
	{
		/* Клиент заблокирован */
		tx_fifo->complete = 1;
		/* Будим всех ждущих принятия пакета */
		wake_up_interruptible(&tx_fifo->tx_wait);
	}
	else
	{
		if( tx_fifo->private_data != NULL )
		{
			tx_fifo->private_data->tx_fifo = NULL;
		}
		/* Освобождаем память */
		kfree(tx_fifo->data);
		kfree(tx_fifo);
	}
}
