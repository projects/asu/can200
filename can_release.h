#ifndef __CAN_RELEASE_H__
#define __CAN_RELEASE_H__

#include <linux/fs.h>

/*
	Реализация функции release для работы с платами CAN-200
*/

//-----------------------------------------------------------------------------
int can_release(struct inode *inode, struct file *filp);
/* Реализация функции release */

#endif /* __CAN_RELEASE_H__ */
