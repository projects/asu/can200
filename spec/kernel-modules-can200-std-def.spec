%define module_name can200
%define module_version 0.1
%define module_release eter1

%define kversion 2.6.27
%define krelease alt16.3
%define flavour std-def

%define module_dir /lib/modules/%kversion-%flavour-%krelease/%module_name

Packager: Alexander Morozov <amorozov@etersoft.ru>

Name: kernel-modules-%module_name-%flavour
Version: %module_version
Release: %module_release

Group: System/Kernel and hardware
Summary: %module_name kernel module
License: GPL

ExclusiveOS: Linux
BuildPreReq: kernel-build-tools >= 0.7
BuildRequires: kernel-headers-modules-%flavour = %kversion-%krelease
BuildRequires: kernel-source-%module_name = %module_version

Provides: kernel-modules-%module_name-%kversion-%flavour-%krelease = %version-%release
Conflicts: kernel-modules-%module_name-%kversion-%flavour-%krelease < %version-%release
Conflicts: kernel-modules-%module_name-%kversion-%flavour-%krelease > %version-%release

Prereq: coreutils
Prereq: modutils
Prereq: kernel-image-%flavour = %kversion-%krelease
Requires(postun): kernel-image-%flavour = %kversion-%krelease

%description
CAN200 driver

%prep
rm -rf kernel-source-%module_name-%module_version
tar jxf %kernel_src/kernel-source-%module_name-%module_version.tar.bz2
%setup -D -T -n kernel-source-%module_name-%module_version

%build
%make

%install
mkdir -p %buildroot/%module_dir
cp -a %module_name.ko %buildroot%module_dir

%post
%post_kernel_modules %kversion-%flavour-%krelease
%postun
%postun_kernel_modules %kversion-%flavour-%krelease

%files
%module_dir/

%changelog
* Wed Mar 10 2010 Alexander Morozov <amorozov@etersoft.ru> 0.1-eter1
- initial build
