#ifndef __CAN_IRQ_H__
#define __CAN_IRQ_H__

#include <linux/interrupt.h>
#include <linux/version.h>

/*
	Реализация функции обслуживания прерывания для работы с платами CAN-200
*/

//-----------------------------------------------------------------------------
#if LINUX_VERSION_CODE > KERNEL_VERSION(2, 6, 18)
irqreturn_t can_irq(int irq, void *dev_id);
#else
irqreturn_t can_irq(int irq, void *dev_id, struct pt_regs *notused);
#endif
/* Реализация функции обслуживания прерывания */

//-----------------------------------------------------------------------------
void transmit_message(struct can_t *can, int channel);
/* Выдача сообщения по шине CAN с проверкой соответствия режима и счетчика повторов */

//-----------------------------------------------------------------------------
void write_replay_error(struct can_t *can, int channel);
/* Разблокирование клиента в случае ошибки */

#endif /* __CAN_IRQ_H__ */
