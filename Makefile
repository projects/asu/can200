obj-m := can200.o
can200-objs := main.o can200pci.o can_open.o can_release.o can_ioctl.o sja1000.o can_write.o can_irq.o can_read.o can_poll.o

EXTRA_CFLAGS = -Wall
KERNELDIR = /usr/src/linux-2.6.27-std-def
PWD := $(PWD)

all:
	$(MAKE) -C $(KERNELDIR) SUBDIRS=$(PWD) modules

clean:
	-rm -rf *.o *.ko .*.cmd .*.flags *.mod.c Module.symvers *.c~ Makefile~ *.h~

