%define module_name can200
%define module_version 0.1
%define module_release eter2
%define module_source %module_name-%module_version.tar.bz2
%define module_source_dir %module_name-%module_version

Name: kernel-source-%module_name
Version: %module_version
Release: %module_release

Summary: Linux %module_name module sources
License: GPL
Group: Development/Kernel
BuildArch: noarch

Source: %module_source

Packager: Alexander Morozov <amorozov@etersoft.ru>

BuildPreReq: kernel-build-tools >= 0.1-alt3

%description
Linux %module_name module sources

%prep
%setup -n %module_source_dir

%install
mkdir -p %buildroot%kernel_src

cd ..
mv %module_source_dir kernel-source-%module_name-%module_version
tar -c kernel-source-%module_name-%module_version | bzip2 -c > \
    %buildroot%kernel_src/kernel-source-%module_name-%module_version.tar.bz2
rm -fr kernel-source-%module_name-%module_version

%files
%kernel_src/kernel-source-%module_name-%module_version.tar.bz2

%changelog
* Tue Mar 10 2010 Alexander Morozov <amorozov@etersoft.ru> 0.1-eter1
- initial build
