#ifndef __CAN_WRITE_H__
#define __CAN_WRITE_H__

#include <linux/fs.h>

/*
	Реализация функции write для работы с платами CAN-200
*/

//-----------------------------------------------------------------------------
ssize_t can_write(struct file *filp, const char *buf, size_t count, loff_t *f_pos);
/* Реализация функции write */

#endif /* __CAN_WRITE_H__ */
