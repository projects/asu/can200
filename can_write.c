#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/fs.h>
#include <linux/uaccess.h>

#include "can_codes.h"
#include "sja1000.h"
#include "can.h"
#include "can_irq.h"
#include "can_ioctl.h"

//-----------------------------------------------------------------------------
ssize_t can_write(struct file *filp, const char *buf, size_t count, loff_t *f_pos)
{
	/* Реализация функции write */
	int dev, channel, port;
	size_t len = 0;
	int res;
	struct can_tx_fifo_t *tx_fifo, *tx_fifo_tmp;
	int blocking;
	unsigned long flags;
	struct private_data_t *private_data;
	CAN_data *data;

	private_data = (struct private_data_t *)(filp->private_data);
	dev = private_data->num_dev;
	channel = private_data->num_channel;

	blocking = !(filp->f_flags & O_NONBLOCK);
	port = can[dev].wPorts[channel];

	if( !access_ok(VERIFY_READ, buf, count) )
	{
		return -EINVAL;
	}
	
	if( (count % sizeof(CAN_data)) != 0 )
	{
		/* Длина выдаваемых данных не кратна размеру CAN_data */
		return -EINVAL;
	}

	/* Выделяем память */
	data = (CAN_data *)kmalloc(count, GFP_KERNEL);
	if( data == NULL )
	{
		printk(KERN_ALERT "can200_open ERROR memory allocation.\n");
		return -EFAULT;
	}

	/* Копируем данные */
	res = copy_from_user(data, buf, count);
	if( res != 0 )
	{
		kfree(data);
		return -EFAULT;
	}

	/* Выделяем память */
	tx_fifo = (struct can_tx_fifo_t *)kmalloc(sizeof(struct can_tx_fifo_t), GFP_KERNEL);
	if( tx_fifo == NULL )
	{
		printk(KERN_ALERT "can200_open ERROR memory allocation.\n");
		kfree(data);
		return -EFAULT;
	}

	tx_fifo->data = data;
	tx_fifo->offset = 0;
	tx_fifo->len = count;
	tx_fifo->tx_counter = TX_COUNTER;
	tx_fifo->blocking = blocking;
	tx_fifo->complete = 0;
	init_waitqueue_head(&tx_fifo->tx_wait);
	tx_fifo->private_data = private_data;
	tx_fifo->next = NULL;

	/* Захватываем спин и запрещаем локальные прерывания */
	spin_lock_irqsave(&can[dev].tx_fifo_lock[channel], flags);

	/* Проверка на режим BUS-OFF */
	if( can[dev].bus_mode[channel] == BUS_OFF )
	{
		/* Освобождаем спин и разрешаем локальные прерывания */
		spin_unlock_irqrestore(&can[dev].tx_fifo_lock[channel], flags);

		/* Освобождаем выделенную память */
		kfree(tx_fifo->data);
		kfree(tx_fifo);

		return -ERESTARTSYS;
	}
	
	private_data->tx_fifo = tx_fifo;
	private_data->status = TRANSMIT_WAIT;

	/* Добавляем данные в список выдачи */
	if( can[dev].tx_fifo[channel] == NULL )
	{
		can[dev].tx_fifo[channel] = tx_fifo;
		if( can[dev].stop_trans[channel] != 1 )
		{
			/* Запускаем выдачу */
			transmit_message(&can[dev], channel);
		}
	}
	else
	{
		tx_fifo_tmp = can[dev].tx_fifo[channel];
		while( tx_fifo_tmp->next != NULL )
		{
			tx_fifo_tmp = tx_fifo_tmp->next;
		}
		tx_fifo_tmp->next = tx_fifo;
	}

	/* Освобождаем спин и разрешаем локальные прерывания */
	spin_unlock_irqrestore(&can[dev].tx_fifo_lock[channel], flags);

	if( tx_fifo->blocking )
	{
		if( wait_event_interruptible(tx_fifo->tx_wait, tx_fifo->complete != 0) )
		{
			down(&can[dev].ioctl_mutex[channel]);

			/* Останавливаем выдачу */
			stop_trans(&can[dev], channel);

			/* Захватываем спин и запрещаем локальные прерывания */
			spin_lock_irqsave(&can[dev].tx_fifo_lock[channel], flags);

			/* Удаляем буфер из очереди */
			if( can[dev].tx_fifo[channel] != NULL )
			{
				if( can[dev].tx_fifo[channel] == tx_fifo )
				{
					/* Текущий выдаваемый буфер */
					can[dev].tx_fifo[channel] = can[dev].tx_fifo[channel]->next;
				}
				else
				{
					tx_fifo_tmp = can[dev].tx_fifo[channel];
					while( tx_fifo_tmp->next != NULL )
					{
						if( tx_fifo_tmp->next == tx_fifo )
						{
							tx_fifo_tmp->next = tx_fifo_tmp->next->next;
							break;
						}
						tx_fifo_tmp = tx_fifo_tmp->next;
					}
				}
			}
			private_data->tx_fifo = NULL;

			/* Освобождаем спин и разрешаем локальные прерывания */
			spin_unlock_irqrestore(&can[dev].tx_fifo_lock[channel], flags);

			start_trans(&can[dev], channel);

			up(&can[dev].ioctl_mutex[channel]);

			if( tx_fifo->offset != 0 )
			{
				len = tx_fifo->offset;
			}
			else
			{
				len = -ERESTARTSYS;
			}

			/* Освобождаем выделенную память */
			kfree(tx_fifo->data);
			kfree(tx_fifo);

			return len;
		}
		/* Захватываем спин и запрещаем локальные прерывания */
		spin_lock_irqsave(&can[dev].tx_fifo_lock[channel], flags);

		private_data->tx_fifo = NULL;

		/* Освобождаем спин и разрешаем локальные прерывания */
		spin_unlock_irqrestore(&can[dev].tx_fifo_lock[channel], flags);

		len = tx_fifo->offset;

		/* Освобождаем выделенную память */
		kfree(tx_fifo->data);
		kfree(tx_fifo);
	}
	else
	{
		len = count;
	}

	return len;
}

