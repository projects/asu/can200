#include <linux/fs.h>
#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/ioctl.h>
#include <linux/uaccess.h>

#include "can.h"
#include "can_codes.h"
#include "sja1000.h"
#include "can_ioctl.h"
#include "can_irq.h"

//-----------------------------------------------------------------------------
int can_ioctl(struct inode *inode, struct file *filp, unsigned int cmd, unsigned long arg)
{
	/* Реализация функции ioctl */
	int dev, channel;
	int err = 0;
	unsigned char mode, status, counter;
	unsigned short speed;
	struct b_filter_t b_filter;
	struct p_filter_t p_filter;
	int ret = 0;
	int port;
	struct CAN_info_t info;
	int over_counter;
	struct can_rx_fifo_t *rx_fifo;
	unsigned long flags;
	union CAN_interrupt_t interrupt;
	struct private_data_t *private_data;
	struct transmit_status_t sts;

	private_data = (struct private_data_t *)(filp->private_data);
	dev = private_data->num_dev;
	channel = private_data->num_channel;
	port = can[dev].wPorts[channel];
	rx_fifo = private_data->rx_fifo;

	if( _IOC_TYPE(cmd) != CAN200_IOC_MAGIC )
	{
		return -ENOTTY;
	}

	if( _IOC_DIR(cmd) & _IOC_READ )
	{
		err = !access_ok(VERIFY_WRITE, (void *)arg, _IOC_SIZE(cmd));
	}
	else
	{
		if( _IOC_DIR(cmd) & _IOC_WRITE )
		{
			err = !access_ok(VERIFY_READ, (void *)arg, _IOC_SIZE(cmd));
		}
	}
	if( err )
	{
		return -EFAULT;
	}

	down(&can[dev].ioctl_mutex[channel]);

	switch( cmd )
	{
	case CAN200_HARDRESET:
		{
			if( can[dev].type == CAN200PC )
			{
				/* Останавливаем выдачу */
				stop_trans(&can[dev], channel);

				/* Захватываем спин и запрещаем локальные прерывания */
				spin_lock_irqsave(&can[dev].irq_lock, flags);
				
				/* Сброс */
				hard_reset(port);
				
				/* Освобождаем клиентов заблокированных на выдаче */
				/* Захватываем спин и запрещаем локальные прерывания */
				spin_lock_irqsave(&can[dev].tx_fifo_lock[channel], flags);
				
				while( can[dev].tx_fifo[channel] != NULL )
				{
					/* Разблокируем всех клиентов */
					write_replay_error(&can[dev], channel);
				}

				/* Освобождаем спин и разрешаем локальные прерывания */
				spin_unlock_irqrestore(&can[dev].tx_fifo_lock[channel], flags);

				/* Настраиваем перрывания */
				interrupt.byte = 0;
				interrupt.bit.ri = 1;
				interrupt.bit.ti = 1;
				interrupt.bit.doi = 1;
				interrupt.bit.ei = 1;

				set_interrupt(port, interrupt.byte); /* Разрешаем прерывания для канала */

				/* Переводим канал в рабочий режим */
				reset_request(port, NormalMode);

				/* Освобождаем спин и разрешаем локальные прерывания */
				spin_unlock_irqrestore(&can[dev].irq_lock, flags);

				start_trans(&can[dev], channel);

			}
			break; 
		}
	case CAN200_GETWORKMODE:
		{
			mode = get_work_mode(port);
			ret = __put_user(mode, (unsigned char *)arg);
			break;
		}
	case CAN200_SETWORKMODE:
		{
			ret = __get_user(mode, (unsigned char *)arg);
			if( ret == 0 )
			{
				/* Останавливаем выдачу */
				stop_trans(&can[dev], channel);

				/* Захватываем спин и запрещаем локальные прерывания */
				spin_lock_irqsave(&can[dev].irq_lock, flags);

				/* Сбрасываем канал */
				reset_request(port, ResetMode);
				
				/* Устанавливаем требуемый режим работы */
				set_work_mode(port, mode);
				
				/* Переводим канал в рабочий режим */
				reset_request(port, NormalMode);
				
				can[dev].bus_mode[channel] = BUS_ON;
				
				/* Освобождаем спин и разрешаем локальные прерывания */
				spin_unlock_irqrestore(&can[dev].irq_lock, flags);

				start_trans(&can[dev], channel);
			}
			break;
		}
	case CAN200_GETCONFIG:
		{
			can_get_info(&info, dev, channel);
			ret = copy_to_user((struct can_info_t *)arg, &info, sizeof(struct CAN_info_t));
			break;
		}
	case CAN200_SETDRIVERMODE:
		{
			/* Останавливаем выдачу */
			stop_trans(&can[dev], channel);

			/* Захватываем спин и запрещаем локальные прерывания */
			spin_lock_irqsave(&can[dev].irq_lock, flags);
			
			/* Сбрасываем канал */
			reset_request(port, ResetMode);
			
			/* Разрешаем прием/выдачу */
			set_driver_mode(port);
				
			/* Переводим канал в рабочий режим */
			reset_request(port, NormalMode);
			
			can[dev].bus_mode[channel] = BUS_ON;

			/* Освобождаем спин и разрешаем локальные прерывания */
			spin_unlock_irqrestore(&can[dev].irq_lock, flags);
			
			start_trans(&can[dev], channel);

			break;
		}
	case CAN200_SETCANSPEED:
		{
			ret = __get_user(speed, (unsigned short *)arg);
			if( ret == 0 )
			{
				/* Останавливаем выдачу */
				stop_trans(&can[dev], channel);
				
				/* Захватываем спин и запрещаем локальные прерывания */
				spin_lock_irqsave(&can[dev].irq_lock, flags);
				
				/* Сбрасываем канал */
				reset_request(port, ResetMode);
				
				/* Устанавливаем скорость */
				set_CAN_speed(port, speed);
				
				/* Переводим канал в рабочий режим */
				reset_request(port, NormalMode);
				
				can[dev].bus_mode[channel] = BUS_ON;
				
				/* Освобождаем спин и разрешаем локальные прерывания */
				spin_unlock_irqrestore(&can[dev].irq_lock, flags);

				start_trans(&can[dev], channel);
			}
			break;
		}
	case CAN200_GETCANSPEED:
		{
			speed = get_CAN_speed(port);
			ret = __put_user(speed, (unsigned short *)arg);
			break;
		}
	case CAN200_GETSTATUS:
		{
			status = get_status(port);
			ret = __put_user(status, (unsigned char *)arg);
			break;
		}
	case CAN200_B_SETFILTER:
		{
			ret = copy_from_user(&b_filter, (struct b_filter_t *)arg, sizeof(struct b_filter_t));
			if( ret == 0 )
			{
				/* Останавливаем выдачу */
				stop_trans(&can[dev], channel);

				/* Захватываем спин и запрещаем локальные прерывания */
				spin_lock_irqsave(&can[dev].irq_lock, flags);
				
				/* Сбрасываем канал */
				reset_request(port, ResetMode);
				
				/* Устанавливаем фильтр */
				ret = b_set_filter(port, &b_filter);
				if( ret != 0 )
				{
					ret = -EPERM;
				}
				
				/* Переводим канал в рабочий режим */
				reset_request(port, NormalMode);
				
				can[dev].bus_mode[channel] = BUS_ON;
				
				/* Освобождаем спин и разрешаем локальные прерывания */
				spin_unlock_irqrestore(&can[dev].irq_lock, flags);

				start_trans(&can[dev], channel);
			}
			break;
		}
	case CAN200_B_GETFILTER:
		{
			/* Останавливаем выдачу */
			stop_trans(&can[dev], channel);

			/* Захватываем спин и запрещаем локальные прерывания */
			spin_lock_irqsave(&can[dev].irq_lock, flags);
			
			/* Сбрасываем канал */
			reset_request(port, ResetMode);
			
			/* Получаем фильтр */
			ret = b_get_filter(port, &b_filter);
			if( ret != 0 )
			{
				ret = -EPERM;
			}
			else
			{
				ret = copy_to_user((struct b_filter_t *)arg, &b_filter, sizeof(struct b_filter_t));
			}
			
			/* Переводим канал в рабочий режим */
			reset_request(port, NormalMode);
				
			can[dev].bus_mode[channel] = BUS_ON;
				
			/* Освобождаем спин и разрешаем локальные прерывания */
			spin_unlock_irqrestore(&can[dev].irq_lock, flags);

			start_trans(&can[dev], channel);
	
			break;
		}
	case CAN200_P_SETFILTER:
		{
			ret = copy_from_user(&p_filter, (struct p_filter_t *)arg, sizeof(struct p_filter_t));
			if( ret == 0 )
			{
				/* Останавливаем выдачу */
				stop_trans(&can[dev], channel);

				/* Захватываем спин и запрещаем локальные прерывания */
				spin_lock_irqsave(&can[dev].irq_lock, flags);
				
				/* Сбрасываем канал */
				reset_request(port, ResetMode);
				
				/* Устанавливаем фильтр */
				ret = p_set_filter(port, &p_filter);
				if( ret != 0 )
				{
					ret = -EPERM;
				}
				
				/* Переводим канал в рабочий режим */
				reset_request(port, NormalMode);
				
				can[dev].bus_mode[channel] = BUS_ON;
				
				/* Освобождаем спин и разрешаем локальные прерывания */
				spin_unlock_irqrestore(&can[dev].irq_lock, flags);
			
				start_trans(&can[dev], channel);
			}
			break;
		}
	case CAN200_P_GETFILTER:
		{
			/* Останавливаем выдачу */
			stop_trans(&can[dev], channel);

			/* Захватываем спин и запрещаем локальные прерывания */
			spin_lock_irqsave(&can[dev].irq_lock, flags);
			
			/* Сбрасываем канал */
			reset_request(port, ResetMode);
			
			/* Получаем фильтр */
			ret = p_get_filter(port, &p_filter);
			if( ret != 0 )
			{
				ret = -EPERM;
			}
			else
			{
				ret = copy_to_user((struct p_filter_t *)arg, &p_filter, sizeof(struct p_filter_t));
			}
			
			/* Переводим канал в рабочий режим */
			reset_request(port, NormalMode);
				
			can[dev].bus_mode[channel] = BUS_ON;
				
			/* Освобождаем спин и разрешаем локальные прерывания */
			spin_unlock_irqrestore(&can[dev].irq_lock, flags);

			start_trans(&can[dev], channel);

			break;
		}
	case CAN200_P_GETRXERRORCOUNTER:
		{
			ret = get_rx_error_counter(port, &counter);
			if( ret != 0 )
			{
				ret = -EPERM;
			}
			else
			{
				ret = copy_to_user((unsigned char *)arg, &counter, sizeof(unsigned char));
			}
			break;
		}
	case CAN200_P_SETRXERRORCOUNTER:
		{
			ret = copy_from_user(&counter, (unsigned char *)arg, sizeof(unsigned char));
			if( ret == 0 )
			{
				/* Останавливаем выдачу */
				stop_trans(&can[dev], channel);

				/* Захватываем спин и запрещаем локальные прерывания */
				spin_lock_irqsave(&can[dev].irq_lock, flags);
				
				/* Сбрасываем канал */
				reset_request(port, ResetMode);
				
				/* Устанавливаем счетчик ошибок приема */
				ret = set_rx_error_counter(port, counter);
				if( ret != 0 )
				{
					ret = -EPERM;
				}
				
				/* Переводим канал в рабочий режим */
				reset_request(port, NormalMode);
				
				can[dev].bus_mode[channel] = BUS_ON;
				
				/* Освобождаем спин и разрешаем локальные прерывания */
				spin_unlock_irqrestore(&can[dev].irq_lock, flags);

				start_trans(&can[dev], channel);
			}
			break;
		}
	case CAN200_P_GETTXERRORCOUNTER:
		{
			ret = get_tx_error_counter(port, &counter);
			if( ret != 0 )
			{
				ret = -EPERM;
			}
			else
			{
				ret = copy_to_user((unsigned char *)arg, &counter, sizeof(unsigned char));
			}
			break;
		}
	case CAN200_P_SETTXERRORCOUNTER:
		{
			ret = copy_from_user(&counter, (unsigned char *)arg, sizeof(unsigned char));
			if( ret == 0 )
			{
				/* Останавливаем выдачу */
				stop_trans(&can[dev], channel);

				/* Захватываем спин и запрещаем локальные прерывания */
				spin_lock_irqsave(&can[dev].irq_lock, flags);
				
				/* Сбрасываем канал */
				reset_request(port, ResetMode);
				
				/* Устанавливаем счетчик ошибок выдачи */
				ret = set_tx_error_counter(port, counter);
				if( ret != 0 )
				{
					ret = -EPERM;
				}
				
				/* Переводим канал в рабочий режим */
				reset_request(port, NormalMode);
				
				can[dev].bus_mode[channel] = BUS_ON;
				
				/* Освобождаем спин и разрешаем локальные прерывания */
				spin_unlock_irqrestore(&can[dev].irq_lock, flags);

				start_trans(&can[dev], channel);
			}
			break;
		}
	case CAN200_P_GETERRORWARNINGLIMIT:
		{
			ret = get_error_warning_limit(port, &counter);
			if( ret != 0 )
			{
				ret = -EPERM;
			}
			else
			{
				ret = copy_to_user((unsigned char *)arg, &counter, sizeof(unsigned char));
			}
			break;
		}
	case CAN200_P_SETERRORWARNINGLIMIT:
		{
			ret = copy_from_user(&counter, (unsigned char *)arg, sizeof(unsigned char));
			if( ret == 0 )
			{
				/* Останавливаем выдачу */
				stop_trans(&can[dev], channel);

				/* Захватываем спин и запрещаем локальные прерывания */
				spin_lock_irqsave(&can[dev].irq_lock, flags);
				
				/* Сбрасываем канал */
				reset_request(port, ResetMode);
				
				/* Устанавливаем счетчик предела ошибок */
				ret = set_error_warning_limit(port, counter);
				if( ret != 0 )
				{
					ret = -EPERM;
				}
				
				/* Переводим канал в рабочий режим */
				reset_request(port, NormalMode);
				
				can[dev].bus_mode[channel] = BUS_ON;
				
				/* Освобождаем спин и разрешаем локальные прерывания */
				spin_unlock_irqrestore(&can[dev].irq_lock, flags);

				start_trans(&can[dev], channel);
			}
			break;
		}
	case CAN200_GETOVERCOUNTER:
		{
			over_counter = rx_fifo->overrun;
			ret = copy_to_user((int *)arg, &over_counter, sizeof(int));
			break;
		}
	case CAN200_SETBUSON:
		{
			if( can[dev].bus_mode[channel] == BUS_OFF )
			{
				/* Останавливаем выдачу */
				stop_trans(&can[dev], channel);

				/* Захватываем спин и запрещаем локальные прерывания */
				spin_lock_irqsave(&can[dev].irq_lock, flags);
				
				/* Сбрасываем канал */
				reset_request(port, ResetMode);
				
				/* Переводим канал в рабочий режим */
				reset_request(port, NormalMode);

				can[dev].bus_mode[channel] = BUS_ON;
				
				/* Освобождаем спин и разрешаем локальные прерывания */
				spin_unlock_irqrestore(&can[dev].irq_lock, flags);

				start_trans(&can[dev], channel);
			}
			break;
		}
	case CAN200_GETTRANSMITSTATUS:
		{
			sts.status = private_data->status;
			sts.len = private_data->len;
			ret = copy_to_user((struct transmit_status_t *)arg, &sts, sizeof(struct transmit_status_t));
			break;
		}
	default:
		{
			ret = -ENOTTY;
			break;
		}
	}

	up(&can[dev].ioctl_mutex[channel]);

	return ret;
}

//-----------------------------------------------------------------------------
void stop_trans(struct can_t *can, int channel)
{
	/* Остановка выдачи заданного канала заданной платы */
	unsigned long flags;
	int res;
	int port;

	port = can->wPorts[channel];

	/* Захватываем спин и запрещаем локальные прерывания */
	spin_lock_irqsave(&can->tx_fifo_lock[channel], flags);

	can->stopping[channel] = 0; /* Признак остановки выдачи */
	can->stop_trans[channel] = 1; /* Команда на остановку выдачи */

	if( can->tx_fifo[channel] != NULL )
	{
		/* Освобождаем спин и разрешаем локальные прерывания */
		spin_unlock_irqrestore(&can->tx_fifo_lock[channel], flags);
		
		res = wait_event_timeout(can->tx_queue[channel], can->stopping[channel] != 0, HZ / 2);
		if( res == 0 )
		{
			/* timeout */
			set_command(port, 0x02);
		}
	}
	else
	{
		/* Освобождаем спин и разрешаем локальные прерывания */
		spin_unlock_irqrestore(&can->tx_fifo_lock[channel], flags);
	}
	return;
}

//-----------------------------------------------------------------------------
void start_trans(struct can_t *can, int channel)
{
	/* Запуск выдачи заданного канала заданной платы */
	unsigned long flags;

	/* Захватываем спин и запрещаем локальные прерывания */
	spin_lock_irqsave(&can->tx_fifo_lock[channel], flags);

	can->stop_trans[channel] = 0;
	can->stopping[channel] = 0;

 	/* Запускаем выдачу */
	transmit_message(can, channel);

	/* Освобождаем спин и разрешаем локальные прерывания */
	spin_unlock_irqrestore(&can->tx_fifo_lock[channel], flags);

	return;
}
