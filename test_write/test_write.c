#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/select.h>
#include <time.h>

#include "can200_api.h"

//-----------------------------------------------------------------------------
void async_trans(int f, int type);
/* ������������� ������ ������� */

//-----------------------------------------------------------------------------
void sync_trans(int f, int type);
/* ����������� ������ ������� */

//-----------------------------------------------------------------------------
int main(int argc, char *argv[])
{
	char file[256];
	int number, f, sync;
	int res;
	struct CAN_info_t info;
	unsigned char type;
	int speed, transmit_type;

	if( argc < 6 )
	{
		printf("Use test_write <number_device> <type> <speed> <sync> <transmit_type>\n");
		return -1;
	}

	sscanf(argv[1], "%d", &number);

	res = strcmp(argv[2], "BasicCAN");
	if( res == 0 )
	{
		type = BasicCAN;
	}
	else
	{
		res = strcmp(argv[2], "PeliCAN");
		if( res == 0 )
		{
			type = PeliCAN;
		}
		else
		{
			printf("type != PeliCAN | BaicCAN\n");
			return -1;
		}
	}

	sscanf(argv[3], "%d", &speed);

	res = strcmp(argv[4], "block");
	if( res == 0 )
	{
		sync = 1;
	}
	else
	{
		res = strcmp(argv[4], "nonblock");
		if( res == 0 )
		{
			sync = 0;
		}
		else
		{
			printf("sync != block | nonblock\n");
			return -1;
		}
	}

	res = strcmp(argv[5], "BasicCAN");
	if( res == 0 )
	{
		transmit_type = BasicCAN;
	}
	else
	{
		res = strcmp(argv[5], "PeliCAN");
		if( res == 0 )
		{
			transmit_type = PeliCAN;
		}
		else
		{
			printf("transmit_type != PeliCAN | BaicCAN\n");
			return -1;
		}
	}

	sprintf(file, "/dev/can200_%d", number);
	if( sync == 0 )
	{
		f = open(file, O_RDWR | O_NONBLOCK);
	}
	else
	{
		f = open(file, O_RDWR);
	}
	if( f == -1 )
	{
		printf("Error open file %s (%d)\n", file, errno);
		return -1;
	}

	res = CAN200_GetConfig(f, &info);
	if( res < 0 )
	{
		printf("Error CAN200_GetConfig\n");
		close(f);
		return -1;
	}
	printf("Name = %s\n", info.szName);
	printf("SN = %s\n", info.szSN);
	printf("IRQ = %d\n", info.wIRQ);
	printf("Ports = 0x%x\n", info.wPorts);

	res = CAN200_SetWorkMode(f, PeliCAN);
	if( res < 0 )
	{
		printf("Error CAN200_SetWorkMode.\n");
		close(f);
		return -1;
	}

	res = CAN200_SetCANSpeed(f, speed);
	if( res < 0 )
	{
		printf("Error CAN200_SetCANSpeed.\n");
		close(f);
		return -1;
	}

	res = CAN200_SetDriverMode(f);
	if( res < 0 )
	{
		printf("Error CAN200_SetDriverMode.\n");
		close(f);
		return -1;
	}
	
	if( sync == 0 )
	{
		async_trans(f, transmit_type);
	}
	else
	{
		sync_trans(f, transmit_type);
	}

	close(f);
	
	return 0;
}

//-----------------------------------------------------------------------------
void async_trans(int f, int type)
{
	/* ������������� ������ ������� */
	CAN_data data;
	int res;
	fd_set wfd;
	struct timeval tv;
	union CAN_status_t status;
	int transmit_status, len;
	unsigned long count;

	data.type = type;
	data.id = 0;
	data.dlc = 0;
	data.rtr = 0;
	
	count = 0;

	while( 1 )
	{
		FD_ZERO(&wfd);
		FD_SET(f, &wfd);

		tv.tv_sec = 1;
		tv.tv_usec = 0;

		res = select(1 + f, NULL, &wfd, NULL, &tv);
		if( res == -1 )
		{
			printf("Error select (%d)\n", errno);
			continue;
		}
		
		if( FD_ISSET(f, &wfd) )
		{
			res = write(f, &data, sizeof(CAN_data));
			if( res < 0 )
			{
				CAN200_GetStatus(f, &status.byte);
				if( status.bit.bs == 1 )
				{
					/* ���� � ��������� buss-off */
					CAN200_SetBusOn(f);
				}
			}
			if( res == sizeof(CAN_data) )
			{
				while( 1 )
				{
					/* ������� ��������� ������ */
					CAN200_GetTransmitStatus(f, &transmit_status, &len);
					if( transmit_status == TRANSMIT_COMPLETE )
					{
						data.id = data.id + 1;
						if( type == PeliCAN )
						{
							if( data.id > 0x1fffffff )
							{
								data.id = 0;
							}
						}
						else
						{
							if( data.id > 0x7ff )
							{
								data.id = 0;
							}
						}
						count = count + 1;
						printf("transmit %lu\r", count);
						fflush(stdout);
						break;
					}
					else
					{
						if( transmit_status == TRANSMIT_ERROR )
						{
							break;
						}
					}
				}
			}
		}
	}

	return;
}

//-----------------------------------------------------------------------------
void sync_trans(int f, int type)
{
	/* ����������� ������ ������� */
	CAN_data data;
	int res;
	union CAN_status_t status;
	unsigned long count;

	data.type = type;
	data.id = 0;
	data.dlc = 0;
	data.rtr = 0;

	count = 0;

	while( 1 )
	{
		res = write(f, &data, sizeof(CAN_data));
		if( res < 0 )
		{
			CAN200_GetStatus(f, &status.byte);
			if( status.bit.bs == 1 )
			{
				/* ���� � ��������� buss-off */
				CAN200_SetBusOn(f);
			}
			continue;
		}
		else
		{
			if( res == sizeof(CAN_data) )
			{
				data.id = data.id + 1;
				if( type == PeliCAN )
				{
					if( data.id > 0x1fffffff )
					{
						data.id = 0;
					}
				}
				else
				{
					if( data.id > 0x7ff )
					{
						data.id = 0;
					}
				}
				count = count + 1;
				printf("transmit %lu\r", count);
				fflush(stdout);
			}
		}
	}

	return;
}
