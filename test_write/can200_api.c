#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include "can200_api.h"

//-----------------------------------------------------------------------------
int CAN200_GetConfig(int fd, struct CAN_info_t *info)
{
	/* Получение режима работы платы */
	int res;

	res = ioctl(fd, CAN200_GETCONFIG, info);

	return res;
}

//-----------------------------------------------------------------------------
int CAN200_SetWorkMode(int fd, unsigned char mode)
{
	/* Установка режима работы платы */
	int res;

	res = ioctl(fd, CAN200_SETWORKMODE, &mode);

	return res;
}

//-----------------------------------------------------------------------------
int CAN200_GetWorkMode(int fd, unsigned char *mode)
{
	/* Получение режима работы платы */
	int res;

	res = ioctl(fd, CAN200_GETWORKMODE, mode);

	return res;
}

//-----------------------------------------------------------------------------
int CAN200_HardReset(int fd)
{
	/* Аппаратный сброс платы */
	int res;

	res = ioctl(fd, CAN200_HARDRESET);

	return res;
}

//-----------------------------------------------------------------------------
int CAN200_SetDriverMode(int fd)
{
	/* Установка разрешения приема/выдачи */
	int res;

	res = ioctl(fd, CAN200_SETDRIVERMODE);

	return res;
}

//-----------------------------------------------------------------------------
int CAN200_SetCANSpeed(int fd, unsigned short speed)
{
	/* Установка скорости обмена */
	int res;

	res = ioctl(fd, CAN200_SETCANSPEED, &speed);

	return res;
}

//-----------------------------------------------------------------------------
int CAN200_GetCANSpeed(int fd, unsigned short *speed)
{
	/* Получение скорости обмена */
	int res;

	res = ioctl(fd, CAN200_GETCANSPEED, speed);

	return res;
}

//-----------------------------------------------------------------------------
int CAN200_GetStatus(int fd, unsigned char *status)
{
	/* Получение статуса канала платы */
	int res;

	res = ioctl(fd, CAN200_GETSTATUS, status);

	return res;
}

//-----------------------------------------------------------------------------
int CAN200_GetOverCounter(int fd, int *counter)
{
	/* Получение счетчика переполнений приема */
	int res;
	
	res = ioctl(fd, CAN200_GETOVERCOUNTER, counter);
	
	return res;
}

//-----------------------------------------------------------------------------
int CAN200_B_SetFilter(int fd, unsigned char mask, unsigned char filter)
{
	/* Установка фильтра режима BasicCAN */
	int res;
	struct b_filter_t b_filter;
	
	b_filter.mask = mask;
	b_filter.filter = filter;

	res = ioctl(fd, CAN200_B_SETFILTER, &b_filter);

	return res;
}

//-----------------------------------------------------------------------------
int CAN200_B_GetFilter(int fd, unsigned char *mask, unsigned char *filter)
{
	/* Получение фильтра режима BasicCAN */
	int res;
	struct b_filter_t b_filter;
	
	res = ioctl(fd, CAN200_B_GETFILTER, &b_filter);
	
	if( res < 0 )
	{
		return res;
	}

	*mask = b_filter.mask;
	*filter = b_filter.filter;

	return res;
}

//-----------------------------------------------------------------------------
int CAN200_P_SetRxErrorCounter(int fd, unsigned char counter)
{
	/* Установка счетчика ошибок приема режима PeliCAN */
	int res;
	
	res = ioctl(fd, CAN200_P_SETRXERRORCOUNTER, &counter);
	
	return res;
}

//-----------------------------------------------------------------------------
int CAN200_P_GetRxErrorCounter(int fd, unsigned char *counter)
{
	/* Получение счетчика ошибок приема режима PeliCAN */
	int res;
	
	res = ioctl(fd, CAN200_P_GETRXERRORCOUNTER, counter);
	
	return res;
}

//-----------------------------------------------------------------------------
int CAN200_P_SetTxErrorCounter(int fd, unsigned char counter)
{
	/* Установка счетчика ошибок выдачи режима PeliCAN */
	int res;
	
	res = ioctl(fd, CAN200_P_SETTXERRORCOUNTER, &counter);
	
	return res;
}

//-----------------------------------------------------------------------------
int CAN200_P_GetTxErrorCounter(int fd, unsigned char *counter)
{
	/* Получение счетчика ошибок выдачи режима PeliCAN */
	int res;
	
	res = ioctl(fd, CAN200_P_GETTXERRORCOUNTER, counter);
	
	return res;
}

//-----------------------------------------------------------------------------
int CAN200_P_SetErrorWarningLimit(int fd, unsigned char counter)
{
	/* Установка верхнего предела счетчиков ошибок режима PeliCAN */
	int res;
	
	res = ioctl(fd, CAN200_P_SETERRORWARNINGLIMIT, &counter);
	
	return res;
}

//-----------------------------------------------------------------------------
int CAN200_P_GetErrorWarningLimit(int fd, unsigned char *counter)
{
	/* Получение верхнего предела счетчиков ошибок режима PeliCAN */
	int res;
	
	res = ioctl(fd, CAN200_P_GETERRORWARNINGLIMIT, counter);
	
	return res;
}

//-----------------------------------------------------------------------------
int CAN200_P_SetFilter(int fd, unsigned char mode, unsigned char amr0, unsigned char amr1,
	unsigned char amr2, unsigned char amr3, unsigned char acr0, unsigned char acr1,
	unsigned char acr2, unsigned char acr3)
{
	/* Установка фильтра режима PeliCAN */
	int res;
	struct p_filter_t p_filter;
	
	p_filter.mask[0] = amr0;
	p_filter.mask[1] = amr1;
	p_filter.mask[2] = amr2;
	p_filter.mask[3] = amr3;
	p_filter.filter[0] = acr0;
	p_filter.filter[1] = acr1;
	p_filter.filter[2] = acr2;
	p_filter.filter[3] = acr3;
	p_filter.mode = mode;

	res = ioctl(fd, CAN200_P_SETFILTER, &p_filter);

	return res;
}

//-----------------------------------------------------------------------------
int CAN200_P_GetFilter(int fd, unsigned char *mode, unsigned char *amr0, unsigned char *amr1,
	unsigned char *amr2, unsigned char *amr3, unsigned char *acr0, unsigned char *acr1,
	unsigned char *acr2, unsigned char *acr3)
{
	/* Получение фильтра режима PeliCAN */
	int res;
	struct p_filter_t p_filter;
	
	res = ioctl(fd, CAN200_P_GETFILTER, &p_filter);

	if( res < 0 )
	{
		return res;
	}

	*amr0 = p_filter.mask[0];
	*amr1 = p_filter.mask[1];
	*amr2 = p_filter.mask[2];
	*amr3 = p_filter.mask[3];
	*acr0 = p_filter.filter[0];
	*acr1 = p_filter.filter[1];
	*acr2 = p_filter.filter[2];
	*acr3 = p_filter.filter[3];
	*mode = p_filter.mode;

	return res;
}

//-----------------------------------------------------------------------------
int CAN200_SetBusOn(int fd)
{
	/* Переключение канала в рабочий режим, после buss-off */
	int res;

	res = ioctl(fd, CAN200_SETBUSON);

	return res;
}

//-----------------------------------------------------------------------------
int CAN200_GetTransmitStatus(int fd, int *status, int *len)
{
	/* Получение статуса выдачи пакета */
	int res;
	struct transmit_status_t sts;
	
	res = ioctl(fd, CAN200_GETTRANSMITSTATUS, &sts);

	if( res < 0 )
	{
		return res;
	}

	*status = sts.status;
	*len = sts.len;

	return res;
}

