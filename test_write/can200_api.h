#ifndef __CAN200_API_H__
#define __CAN200_API_H__

/*
	Функции для работы с платами CAN-200
*/

#include "can_codes.h"

//-----------------------------------------------------------------------------
int CAN200_GetConfig(int fd, struct CAN_info_t *info);
/* Получение режима работы платы */

//-----------------------------------------------------------------------------
int CAN200_SetWorkMode(int fd, unsigned char mode);
/* Установка режима работы платы */

//-----------------------------------------------------------------------------
int CAN200_GetWorkMode(int fd, unsigned char *mode);
/* Получение режима работы платы */

//-----------------------------------------------------------------------------
int CAN200_HardReset(int fd);
/* Аппаратный сброс платы */

//-----------------------------------------------------------------------------
int CAN200_SetDriverMode(int fd);
/* Установка скорости обмена */

//-----------------------------------------------------------------------------
int CAN200_SetCANSpeed(int fd, unsigned short speed);
/* Установка скорости обмена */

//-----------------------------------------------------------------------------
int CAN200_GetCANSpeed(int fd, unsigned short *speed);
/* Получение скорости обмена */

//-----------------------------------------------------------------------------
int CAN200_GetStatus(int fd, unsigned char *status);
/* Получение статуса канала платы */

//-----------------------------------------------------------------------------
int CAN200_GetOverCounter(int fd, int *counter);
/* Получение счетчика переполнений приема */

//-----------------------------------------------------------------------------
int CAN200_B_SetFilter(int fd, unsigned char mask, unsigned char filter);
/* Установка фильтра режима BasicCAN */

//-----------------------------------------------------------------------------
int CAN200_B_GetFilter(int fd, unsigned char *mask, unsigned char *filter);
/* Получение фильтра режима BasicCAN */

//-----------------------------------------------------------------------------
int CAN200_P_SetRxErrorCounter(int fd, unsigned char counter);
/* Установка счетчика ошибок приема режима PeliCAN */

//-----------------------------------------------------------------------------
int CAN200_P_GetRxErrorCounter(int fd, unsigned char *counter);
/* Получение счетчика ошибок приема режима PeliCAN */

//-----------------------------------------------------------------------------
int CAN200_P_SetTxErrorCounter(int fd, unsigned char counter);
/* Установка счетчика ошибок выдачи режима PeliCAN */

//-----------------------------------------------------------------------------
int CAN200_P_GetTxErrorCounter(int fd, unsigned char *counter);
/* Получение счетчика ошибок выдачи режима PeliCAN */

//-----------------------------------------------------------------------------
int CAN200_P_SetErrorWarningLimit(int fd, unsigned char counter);
/* Установка верхнего предела счетчиков ошибок режима PeliCAN */

//-----------------------------------------------------------------------------
int CAN200_P_GetErrorWarningLimit(int fd, unsigned char *counter);
/* Получение верхнего предела счетчиков ошибок режима PeliCAN */

//-----------------------------------------------------------------------------
int CAN200_P_SetFilter(int fd, unsigned char mode, unsigned char amr0, unsigned char amr1,
	unsigned char amr2, unsigned char amr3, unsigned char acr0, unsigned char acr1,
	unsigned char acr2, unsigned char acr3);
/* Установка фильтра режима PeliCAN */

//-----------------------------------------------------------------------------
int CAN200_P_GetFilter(int fd, unsigned char *mode, unsigned char *amr0, unsigned char *amr1,
	unsigned char *amr2, unsigned char *amr3, unsigned char *acr0, unsigned char *acr1,
	unsigned char *acr2, unsigned char *acr3);
/* Получение фильтра режима PeliCAN */

//-----------------------------------------------------------------------------
int CAN200_SetBusOn(int fd);
/* Переключение канала в рабочий режим, после buss-off */

//-----------------------------------------------------------------------------
int CAN200_GetTransmitStatus(int fd, int *status, int *len);
/* Получение статуса выдачи пакета */

#endif /* __CAN200_API_H__ */
