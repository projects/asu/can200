#ifndef __CAN_CODES_H__
#define __CAN_CODES_H__

#include <linux/ioctl.h>

#pragma pack(1)

union CAN_status_t
{
	struct
	{
		unsigned char rbs			: 1; /* Статус приемного буфера */
		unsigned char dos			: 1; /* Статус переполнения */
		unsigned char tbs			: 1; /* Статус буфера выдачи */
		unsigned char tcs			: 1; /* Статус завершенности выдачи */
		unsigned char rs			: 1; /* Статус приема */
		unsigned char ts			: 1; /* Статус выдачи */
		unsigned char es			: 1; /* Статус ошибки */
		unsigned char bs			: 1; /* Статус шины */
	} bit;
	unsigned char byte;
};

union CAN_error_t
{
	struct
	{
		unsigned char seg				: 5; /* Положение ошибки в кадре */
		/*
			2 - Идентификатор: биты 28-21
			3 - Начало кадра
			4 - бит SRTR
			5 - бит IDE
			6 - Идентификатор: биты 20-18
			7 - Идентификатор: биты 17-13
			8 - CRC последовательность
			9 - зарезервированный бит 0
			10 - поле данных
			11 - код длины данных
			12 - бит RTR
			13 - зарезервированный бит 1
			14 - Идентификатор: биты 4-0
			15 - Идентификатор: биты 12-5

			17 - флаг активной ошибки
			18 - перерыв на шине
			19 - tolerate dominant bits

			22 - флаг пассивной ошибки
			23 - разделитель ошибки
			24 - разделитель CRC
			25 - интервал подтверждения
			26 - конец кадра
			27 - разделитель подтверждения
			28 - флаг переполнения
		*/
		unsigned char dir				: 1; /* Прием / передача */
		/*
			0 - Передача
			1 - Прием
		*/
		unsigned char errc				: 2; /* Тип ошибки */
		/*
			0 - битовая ошибка
			1 - ошибка формы
			2 - ошибка заполнения
			3 - другая ошибка
		*/
	} bit;
	unsigned char byte;
};

/* Структура для выдачи и приема данных */
typedef struct
{
	unsigned char type;			/* 0 - стандартный кадр, 1 - расширенный кадр, 2 - ошибка */
	unsigned long id;			/* Идентификатор (11-битный стандартный или 29-битный расширенный) */
	unsigned char rtr;			/* RTR бит кадра */
	unsigned char dlc;			/* Количество байт данных в кадре */
	unsigned char data[8];			/* Данные */
	struct timeval timestamp;		/* Время приема сообщения */
} CAN_data, *pCAN_data;

/* Структура для задания фильтра режима BasicCAN */
struct b_filter_t
{
	unsigned char mask;
	unsigned char filter;
};

/* Структура для задания фильтра режима PeliCAN */
struct p_filter_t
{
	unsigned char mode;
	unsigned char mask[4];
	unsigned char filter[4];
};

/* Структура для получения информации о плате */
struct CAN_info_t
{
	char szName[128];	/* Имя платы */
	char szSN[10];		/* Серийный номер платы */
	int wPorts;		/* Базовый адрес */
	int wIRQ;		/* Прерывание */
};

/* Структура для получения статуса выдачи */
struct transmit_status_t
{
	int status;
	int len;
};

#pragma pack()

/* Текущий статус выдачи */
#define TRANSMIT_WAIT			0
#define TRANSMIT				1
#define TRANSMIT_COMPLETE		2
#define TRANSMIT_ERROR			3

/* Режимы работы каналов плат */
#define BasicCAN	0
#define PeliCAN		1
#define ErrorCAN	2

/* Стандартные значения скорости обмена по шине CAN */
#define CAN_SPEED_1000		1000
#define CAN_SPEED_800		800
#define CAN_SPEED_500		500
#define CAN_SPEED_250		250
#define CAN_SPEED_125		125
#define CAN_SPEED_50		50
#define CAN_SPEED_20		20
#define CAN_SPEED_10		10

/*
	Коды для управления платами (ioctl)
*/

#define CAN200_IOC_MAGIC		'k'

/* Команды управления платами */
#define CAN200_HARDRESET				_IO(CAN200_IOC_MAGIC,  0)	/* Аппаратный сброс контроллера канала платы */	
#define CAN200_SETWORKMODE				_IOW(CAN200_IOC_MAGIC, 1, unsigned char)	/* Установка режима работы канала платы */
#define CAN200_GETWORKMODE				_IOR(CAN200_IOC_MAGIC, 2, unsigned char)	/* Получение режима работы канала платы */
#define CAN200_SETDRIVERMODE			_IO(CAN200_IOC_MAGIC,  3)	/* Настройка выходного формирователя CAN-контроллера */
#define CAN200_GETCANSPEED				_IOR(CAN200_IOC_MAGIC, 4, unsigned short)	/* Получение скорости работы канала платы */
#define CAN200_SETCANSPEED				_IOW(CAN200_IOC_MAGIC, 5, unsigned short)	/* Установка скорости работы канала платы */
#define CAN200_GETSTATUS				_IOR(CAN200_IOC_MAGIC, 6, unsigned char)	/* Получение статуса канала платы */
#define CAN200_GETCONFIG				_IOR(CAN200_IOC_MAGIC, 7, struct CAN_info_t)	/* Получение конфигурации платы */
#define CAN200_B_SETFILTER				_IOW(CAN200_IOC_MAGIC, 8, struct b_filter_t)	/* Установка фильтра режима BasicCAN канала платы */
#define CAN200_P_SETFILTER				_IOW(CAN200_IOC_MAGIC, 9, struct p_filter_t)	/* Установка фильтра режима PeliCAN канала платы */
#define CAN200_P_GETRXERRORCOUNTER		_IOR(CAN200_IOC_MAGIC, 10, unsigned char)	/* Получение счетчика ошибок приема режима PeliCAN канала платы */
#define CAN200_P_SETRXERRORCOUNTER		_IOW(CAN200_IOC_MAGIC, 11, unsigned char)	/* Установка счетчика ошибок приема режима PeliCAN канала платы */
#define CAN200_P_GETTXERRORCOUNTER		_IOR(CAN200_IOC_MAGIC, 12, unsigned char)	/* Получение счетчика ошибок выдача режима PeliCAN канала платы */
#define CAN200_P_SETTXERRORCOUNTER		_IOW(CAN200_IOC_MAGIC, 13, unsigned char)	/* Установка счетчика ошибок выдачи режима PeliCAN канала платы */
#define CAN200_P_GETERRORWARNINGLIMIT	_IOR(CAN200_IOC_MAGIC, 14, unsigned char)	/* Получение верхнего предела счетчиков ошибок режима PeliCAN канала платы */
#define CAN200_P_SETERRORWARNINGLIMIT	_IOW(CAN200_IOC_MAGIC, 15, unsigned char)	/* Установка верхнего предела счетчиков ошибок режима PeliCAN канала платы */
#define CAN200_B_GETFILTER				_IOR(CAN200_IOC_MAGIC, 16, struct b_filter_t)	/* Получение фильтра режима BasicCAN канала платы */
#define CAN200_P_GETFILTER				_IOR(CAN200_IOC_MAGIC, 17, struct p_filter_t)	/* Получение фильтра режима PeliCAN канала платы */
#define CAN200_GETOVERCOUNTER			_IOR(CAN200_IOC_MAGIC, 18, int)	/* Получение счетчика переполнений приема канала платы */
#define CAN200_SETBUSON					_IO(CAN200_IOC_MAGIC,  19) /* Переключение кнала в рабочий режим после BUS_OFF */
#define CAN200_GETTRANSMITSTATUS		_IOR(CAN200_IOC_MAGIC, 20, struct transmit_status_t)	/* Получение статуса выдачи пакета */

#endif /* __CAN_CODES_H__ */
