#ifndef __CAN_OPEN_H__
#define __CAN_OPEN_H__

#include <linux/fs.h>

/*
	Реализация функции open для работы с платами CAN-200
*/

//-----------------------------------------------------------------------------
int can_open(struct inode *inode, struct file *filp);
/* Реализация функции open */

#endif /* __CAN_OPEN_H__ */
