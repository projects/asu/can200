#ifndef __CAN_IOCTL_H__
#define __CAN_IOCTL_H__

#include <linux/fs.h>

/*
	Реализация функции ioctl для работы с платами CAN-200
*/

//-----------------------------------------------------------------------------
int can_ioctl(struct inode *inode, struct file *filp, unsigned int cmd, unsigned long arg);
/* Реализация функции ioctl */
//-----------------------------------------------------------------------------
void stop_trans(struct can_t *can, int channel);
/* Остановка выдачи заданного канала заданной платы */
//-----------------------------------------------------------------------------
void start_trans(struct can_t *can, int channel);
/* Запуск выдачи заданного канала заданной платы */

#endif /* __CAN_IOCTL_H__ */
