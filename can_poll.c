#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/fs.h>
#include <linux/uaccess.h>
#include <linux/poll.h>

#include "can_codes.h"
#include "sja1000.h"
#include "can.h"

//-----------------------------------------------------------------------------
unsigned int can_poll(struct file *filp, struct poll_table_struct *wait)
{
	/* Реализация функции poll */
	unsigned int mask = 0;
	int dev, channel;
	struct can_rx_fifo_t *rx_fifo;
	struct private_data_t *private_data;

	private_data = (struct private_data_t *)(filp->private_data);
	dev = private_data->num_dev;
	channel = private_data->num_channel;
	rx_fifo = private_data->rx_fifo;

	poll_wait(filp, &can[dev].rx_queue[channel], wait);
	poll_wait(filp, &can[dev].tx_queue[channel], wait);

	if( atomic_read(&rx_fifo->len) != 0 )
	{
		mask |= POLLIN | POLLRDNORM;
	}
	if( can[dev].tx_fifo[channel] == NULL )
	{
		mask |= POLLOUT | POLLWRNORM;
	}

	return mask;
}
