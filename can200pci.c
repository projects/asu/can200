#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/pci.h>

#include "can.h"

#define CAN200_DEVICE_ID		0x9030
#define CAN200_VENDOR_ID		0x10b5
#define CAN200_SUB_DEVICE_ID		0x0301
#define CAN200_SUB_VENDOR_ID		0xe1c5

//-----------------------------------------------------------------------------
int find_can200(struct can_t *can)
{
	/* Поиск устройств CAN-200PCI */
	int res;
	int found;
	struct pci_dev *dev;
	void *ports;

	/* Ищем устройства с DeviceID == 0x9030 и VendorID == 0x10b5 */
	found = 0;
	dev = NULL;
	while( found < CAN_MAX_DEVICE )
	{
		dev = pci_get_device(CAN200_VENDOR_ID, CAN200_DEVICE_ID, dev);
		if( dev == NULL )
		{
			break;
		}
		if( (dev->subsystem_vendor == CAN200_SUB_VENDOR_ID) && (dev->subsystem_device == CAN200_SUB_DEVICE_ID) )
		{
			/* Разрешаем устройство */
			res = pci_enable_device(dev);
			if( res != 0 )
			{
				printk(KERN_ALERT "Error pci_enable_device.\n");
				return -1;
			}
			(can + found)->pci_enable_device = 1;

			/* CAN200PCI */
			(can + found)->dev = dev;
			/* Номер прерывания */
			(can + found)->wIrql = dev->irq;
			/* Тип платы */
			(can + found)->type = CAN200PCI;

			/* Порты ввода-вывода двух каналов */

			(can + found)->wPorts[0] = pci_resource_start(dev, 2);
			/* Регистрируем используемые нами порты ввода-вывода */
			ports = request_region((can + found)->wPorts[0], 0x80, "CAN200");
			if( ports == NULL )
			{
				printk(KERN_ALERT "Ports request region error.\n");
				(can + found)->wPorts[0] = 0;
				return -1;
			}

			(can + found)->wPorts[1] = pci_resource_start(dev, 3);
			/* Регистрируем используемые нами порты ввода-вывода */
			ports = request_region((can + found)->wPorts[1], 0x80, "CAN200");
			if( ports == NULL )
			{
				printk(KERN_ALERT "Ports request region error.\n");
				(can + found)->wPorts[1] = 0;
				return -1;
			}
			found = found + 1;
		}
	}

	return found;
}

//-----------------------------------------------------------------------------
int can_get_info(struct CAN_info_t *info, int num_dev, int num_channel)
{
	/* Получение информации о плате */
	int res;
	int i;
	unsigned short reg, wAddr;
	unsigned int dwWait, data[256], cb;
	unsigned char *ptr;

	info->wPorts = can[num_dev].wPorts[num_channel];
	info->wIRQ = can[num_dev].wIrql;
	if( can[num_dev].type != CAN200PCI )
	{
		/* Плата не поддреживает pnp */
		sprintf(info->szName, "CAN-200PC/MP/PC104");
		info->szSN[0] = 0;
	}
	else
	{
		i = 0;
		/* Читаем имя и серийный номер платы */
		for(wAddr = 0x88; wAddr < 0x200; wAddr += 4)
		{
			dwWait = 0;
			/* Записываем адрес */
			res = pci_write_config_word(can[num_dev].dev, 0x4e, wAddr);
			if( res != 0 )
			{
				/* Ошибка записи адреса */
				printk(KERN_ALERT "PCI bus write error (%x).\n", res);
				return -1;
			}
			/* Ждем окончания чтения */
			do
			{
				dwWait = dwWait + 1;
				res = pci_read_config_word(can[num_dev].dev, 0x4e, &reg);
				if( res != 0 )
				{
					/* Ошибка чтения готовности */
					printk(KERN_ALERT "PCI bus read error.\n");
					return -1;
				}
				if( dwWait % 140 == 0 )
				{
					/* Повтор выдачи адреса */
					res = pci_write_config_word(can[num_dev].dev, 0x4e, wAddr);
					if( res != 0 )
					{
						/* Ошибка записи адреса */
						printk(KERN_ALERT "PCI bus write error.\n");
						return -1;
					}
				}
			} while( (reg & 0x8000 ) == 0 );
			/* Читаем данные */
			data[i] = 0;
			res = pci_read_config_dword(can[num_dev].dev, 0x50, &data[i]);
			if( res != 0 )
			{
				/* Ошибка чтения готовности */
				printk(KERN_ALERT "PCI bus read error.\n");
				return -1;
			}
			i = i + 1;
		}
		/* Длина имени */
		cb = (data[0] >> 8) & 0xff;
		ptr = (unsigned char *)&data[0];
		/* Имя платы */
		memcpy(info->szName, ptr + 3, cb);
		info->szName[cb] = 0;
		/* Серийный номер платы */
		cb = 0;
		for(i = 0; i < 80; i++)
		{
			if( *(unsigned short *)(ptr + i) == 0x4e53 )
			{
				cb = *(ptr + i + 2);
				memcpy(info->szSN, ptr + i + 3, cb);
			}
		}
		info->szSN[cb] = 0;
	}

	return 0;
}
