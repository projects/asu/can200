#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/select.h>

#include "can200_api.h"

//-----------------------------------------------------------------------------
void sync_recieve(int f, int type);
/* Блокирующий прием пакетов */
//-----------------------------------------------------------------------------
void async_recieve(int f, int type);
/* Неблокирующий прием пакетов */

//-----------------------------------------------------------------------------
int main(int argc, char *argv[])
{
	char file[256];
	int number, f;
	int res;
	struct CAN_info_t info;
	int type, speed, receive_type;
	int lost;
	int sync;
	unsigned long total;

	if( argc < 6 )
	{
		printf("Use receive <number_device> <type> <speed> <sync> <receive_type>\n");
		return -1;
	}

	sscanf(argv[1], "%d", &number);

	res = strcmp(argv[2], "BasicCAN");
	if( res == 0 )
	{
		type = BasicCAN;
	}
	else
	{
		res = strcmp(argv[2], "PeliCAN");
		if( res == 0 )
		{
			type = PeliCAN;
		}
		else
		{
			printf("type != PeliCAN | BaicCAN\n");
			return -1;
		}
	}

	sscanf(argv[3], "%d", &speed);

	res = strcmp(argv[4], "block");
	if( res == 0 )
	{
		sync = 1;
	}
	else
	{
		res = strcmp(argv[4], "nonblock");
		if( res == 0 )
		{
			sync = 0;
		}
		else
		{
			printf("sync != block | nonblock\n");
			return -1;
		}
	}

	res = strcmp(argv[5], "BasicCAN");
	if( res == 0 )
	{
		receive_type = BasicCAN;
	}
	else
	{
		res = strcmp(argv[5], "PeliCAN");
		if( res == 0 )
		{
			receive_type = PeliCAN;
		}
		else
		{
			printf("receive_type != PeliCAN | BaicCAN\n");
			return -1;
		}
	}

	sprintf(file, "/dev/can200_%d", number);
	if( sync == 0 )
	{
		f = open(file, O_RDWR | O_NONBLOCK);
	}
	else
	{
		f = open(file, O_RDWR);
	}
	if( f == -1 )
	{
		printf("Error open file %s (%d)\n", file, errno);
		return -1;
	}

	res = CAN200_GetConfig(f, &info);
	if( res < 0 )
	{
		printf("Error CAN200_GetConfig\n");
		close(f);
		return -1;
	}
	printf("Name = %s\n", info.szName);
	printf("SN = %s\n", info.szSN);
	printf("IRQ = %d\n", info.wIRQ);
	printf("Ports = 0x%x\n", info.wPorts);

	res = CAN200_SetWorkMode(f, type);
	if( res < 0 )
	{
		printf("Error CAN200_SetWorkMode.\n");
		close(f);
		return -1;
	}

	res = CAN200_SetCANSpeed(f, speed);
	if( res < 0 )
	{
		printf("Error CAN200_SetCANSpeed.\n");
		close(f);
		return -1;
	}

	res = CAN200_SetDriverMode(f);
	if( res < 0 )
	{
		printf("Error CAN200_SetDriverMode.\n");
		close(f);
		return -1;
	} 

	if( type == PeliCAN )
	{
		res = CAN200_P_SetFilter(f, 0, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff);
		if( res < 0 )
		{
			printf("Error CAN200_P_SetFilter, res = %d, errno = %d.\n", res, errno);
			close(f);
			return -1;
		}
	}
	else
	{
		res = CAN200_B_SetFilter(f, 0xff, 0xff);
		if( res < 0 )
		{
			printf("Error CAN200_B_SetFilter, res = %d, errno = %d.\n", res, errno);
			close(f);
			return -1;
		}
	}

	CAN200_GetOverCounter(f, &lost);
	printf("lost = %u\n", lost);
	total = 0;
	
	if( sync == 0 )
	{
		async_recieve(f, receive_type);
	}
	else
	{
		sync_recieve(f, receive_type);
	}

	close(f);

	return EXIT_SUCCESS;
}

//-----------------------------------------------------------------------------
void async_recieve(int f, int type)
{
	/* Неблокирующий прием пакетов */
	int res, i;
	CAN_data data[1024];
	fd_set rfd;
	struct timeval tv;
	int lost;
	unsigned long counter, count, max_counter;
	union CAN_status_t status;
	
	counter = 0;
	count = 0;

	if( type == PeliCAN )
	{
		max_counter = 0x1fffffff;
	}
	else
	{
		max_counter = 0x7ff;
	}

	while( 1 )
	{
		FD_ZERO(&rfd);
		FD_SET(f, &rfd);

		tv.tv_sec = 1;
		tv.tv_usec = 0;

		res = select(1 + f, &rfd, NULL, NULL, &tv);
		if( res == -1 )
		{
			continue;
		}

		if( FD_ISSET(f, &rfd) )
		{
			res = read(f, data, 1024 * sizeof(CAN_data));
			if( res <= 0 )
			{
				/* Ошибка на шине CAN? */
				CAN200_GetStatus(f, &status.byte);
				if( status.bit.bs == 1 )
				{
					/* Шина в состоянии buss-off */
					CAN200_SetBusOn(f);
				}
				continue;
			}
			for(i = 0; i < res / sizeof(CAN_data); i++)
			{
				if( data[i].type == type )
				{
					if( counter != data[i].id )
					{
						/* Определяем число потеряных пакетов */
						CAN200_GetOverCounter(f, &lost);
						printf("\nexpected id 0x%lx, receive 0x%lx, lost = %u (i = %d)\n", counter, data[i].id, lost, i);
						counter = data[i].id;
					}
					counter = counter + 1;
					if( counter > max_counter )
					{
						counter = 0;
					}
					count = count + 1;
				}
				else
				{
					if( data[i].type == ErrorCAN )
					{
						/* Ошибка на шине CAN */
						status.byte = data[i].data[1];
						if( status.bit.bs == 1 )
						{
							/* Шина в состоянии buss-off */
							CAN200_SetBusOn(f);
						}
					}
				}
			}
			printf("receive %lu\r", count);
			fflush(stdout);
		}
	}
	return;
}

//-----------------------------------------------------------------------------
void sync_recieve(int f, int type)
{
	/* Блокирующий прием пакетов */
	int res, i;
	CAN_data data[1000];
	int lost;
	unsigned long counter, count, max_counter;
	union CAN_status_t status;

	counter = 0;
	count = 0;

	if( type == PeliCAN )
	{
		max_counter = 0x1fffffff;
	}
	else
	{
		max_counter = 0x7ff;
	}

	while( 1 )
	{
		res = read(f, data, 1000 * sizeof(CAN_data));
		if( res <= 0 )
		{
			/* Ошибка на шине CAN? */
			CAN200_GetStatus(f, &status.byte);
			if( status.bit.bs == 1 )
			{
				/* Шина в состоянии buss-off */
				CAN200_SetBusOn(f);
			}
			continue;
		}
		for(i = 0; i < res / sizeof(CAN_data); i++)
		{
			if( data[i].type == type )
			{
				if( counter != data[i].id )
				{
					CAN200_GetOverCounter(f, &lost);
					printf("\nexpected id 0x%lx, receive 0x%lx, lost = %u (i = %d)\n", counter, data[i].id, lost, i);
					counter = data[i].id;
				}
				counter = counter + 1;
				if( counter > max_counter )
				{
					counter = 0;
				}
				count = count + 1;
			}
			else
			{
				if( data[i].type == ErrorCAN )
				{
					status.byte = data[i].data[1];
					if( status.bit.bs == 1 )
					{
						/* Шина в состоянии buss-off */
						CAN200_SetBusOn(f);
					}
				}
			}
		}
		printf("receive %lu\r", count);
		fflush(stdout);
	}
	return;
}
