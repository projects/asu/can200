#ifndef __CAN_READ_H__
#define __CAN_READ_H__

#include <linux/fs.h>

/*
	Реализация функции read для работы с платами CAN-200
*/

//-----------------------------------------------------------------------------
ssize_t can_read(struct file *filp, char *buf, size_t count, loff_t *f_pos);
/* Реализация функции read */

//-----------------------------------------------------------------------------
int write_client(char *buf, int size, size_t *len, struct can_rx_fifo_t *rx_fifo);

#endif /* __CAN_READ_H__ */

