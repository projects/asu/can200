#ifndef __CAN_POLL_H__
#define __CAN_POLL_H__

#include <linux/fs.h>

/*
	Реализация функции poll и select для работы с платами CAN-200
*/

//-----------------------------------------------------------------------------
unsigned int can_poll(struct file *filp, struct poll_table_struct *wait);
/* Реализация функции poll */

#endif /* __CAN_POLL_H__ */
